<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Input number : <input type="text" name="n">
  
  <input type="submit">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    
    $n = htmlspecialchars($_REQUEST['n']);
    
    
    if (empty($n)) {
    
        
        echo $n." is undefined";
        
    } else {
        for ($i=1; $i<=$n; $i++) {
            if ($i%3==0 && $i%5==0) {
                echo 'FizzBuzz';
            }else if($i%3==0){
                echo 'Fizz';
            }else if($i%5==0){
                echo 'Buzz';
            }else{
                echo $i;
            }
            echo "\n";
   }

    }
}

    ?>
</body>
</html>