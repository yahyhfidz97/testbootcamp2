<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Leap year</title>
</head>
<body>
    
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  startYear : <input type="text" name="startYear">
  <br>
  endYear : <input type="text" name="endYear">
  <input type="submit">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    
    $startYear = htmlspecialchars($_REQUEST['startYear']);
    $endYear = htmlspecialchars($_REQUEST['endYear']);
    
    if (empty($startYear) && empty($endYear)) {
    
        
        echo "Form Empty";
        
    } else {
    echo "Start Year: ".$startYear," <br>End Year: ".$endYear."<br>";
    	
        $yearsToCheck = range($startYear, $endYear);
   
   foreach ($yearsToCheck as $year) {
       $isLeapYear = (bool) date('L', strtotime("$year-01-01"));
       if($isLeapYear === true) echo $year . ", ";
   }

    }
}

    ?>
</body>
</html>