<?php

$ttl = 56;

nearFib(19);
echo "<br>";
echo "<br>";

nearFib($ttl); 
echo "<br>";
echo "<br>";

echo "Array Fibonanci : <br>";
echo implode(" ", fibonacci(20));

function nearFib($ttl)
{
    $input = $ttl;
    echo "Input : " . $input . "<br>";

    $arrFib = fibonacci(20);
    $flag = 0;
    while ($flag < 1) {
        if (in_array($ttl, $arrFib)) {
            $flag = 1;
        } else {
            $ttl++;
        }
    }

    echo "Output : " . substr($ttl, 0 , 1);

}

function fibonacci (int $jumlahBilangan) {
  # array ini akan digunakan untuk menampung bilangan fibonacci
  $fibonacci = [];

  if ($jumlahBilangan < 0) {
    # langsung hentikan fungsi jika $jumlahBilangan kurang dari 0
    return $fibonacci; 
  }

  # mulai perulangan
  for ($i = 0; $i < $jumlahBilangan; $i++) {
    if ($i < 2) {
      $bilangan = $i;
    } else {
      $bilangan = $fibonacci[$i - 1] + $fibonacci[$i - 2];
    }

    # tambahkan $bilangan ke dalam array $fibonacci
    array_push($fibonacci, $bilangan);
  }

  return $fibonacci;
}

?>