<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Palindrome</title>
</head>
<body>
<?php


function palindrome($word){
if(strtolower($word)==strtolower(strrev($word)))
{
    echo $word;
    echo "    # --> palindrome <br>";
}else{
    echo $word;
    echo "    # --> not palindrome <br>";
}
}


palindrome("Radar") ; 
palindrome("Malam") ; 
palindrome("Kasur ini rusak"); 
palindrome("Ibu Ratna antar ubi");
palindrome("Malas"); 
palindrome("Makan nasi goreng");
palindrome("Balonku ada lima");

?>

</body>

</html>